<?php
/**
 * Drupal HotSpot Module: for use with CoovaChilli:
 * http://coova.org/wiki/index.php/CoovaChilli
 * Copyright 2008 (c) David Bird <david@coova.com>
 * Licensed under the Gnu Public License.
 */

function hotspot_set_session($v) {
  if (isset($_REQUEST[$v])) $_SESSION[$v] = $_REQUEST[$v];
}

function hotspot_get_param($v) {
  if (isset($_REQUEST[$v])) return $_REQUEST[$v];
  return $_SESSION[$v];
}

function hotspot_get_username($input) {
  $username = $input['UserName'];
  if (!$username) $username = $input['Username'];
  if (!$username) $username = $input['username'];
  if ($username) {
    if ($input['realm']) {
      $realm = $input['realm'];
    } else {
      $realm = variable_get('hotspot_realm', false);
    }
    if ($realm) $username = $realm.'/'.$username;
  }
  return $username;
}

function hotspot_get_password($input) {
  $password = $input['Password'];
  if (!$password) $password = $input['password'];
  return $password;
}

function hotspot_get_uamsecret() {
  if ($_SESSION['uamsecret']) 
    return $_SESSION['uamsecret'];
  return variable_get('hotspot_uamsecret', '');
}

function hotspot_raw_challenge() {
  return pack("H32", hotspot_get_param('challenge'));
}

function hotspot_get_challenge() {
  $challenge = hotspot_raw_challenge();
  $uamsecret = hotspot_get_uamsecret();
  if (isset($uamsecret)) {
    $challenge = pack("H*", md5($challenge . $uamsecret));
  } 
  return $challenge;
}

function hotspot_pap_password($pass) {
  $challenge = hotspot_get_challenge();
  $pass = pack("a32", $pass);
  return implode("", unpack("H32", ($pass ^ $challenge)));
}

function hotspot_chap_response($pass) {
  $challenge = hotspot_get_challenge();
  return md5("\0" . $pass . $challenge);
}

function hotspot_login_url($username, $password) {

  if (variable_get('hotspot_authproto', 'chap') == 'chap') {
    
    $response = hotspot_chap_response($password);
    
    $url = 'http://' . 
      hotspot_get_param('uamip') . ':' . 
      hotspot_get_param('uamport') . '/logon?username=' . 
      $username . '&response=' . $response . '&userurl=' . 
	hotspot_get_param('userurl');
    
  } else {
    
    $pappassword = hotspot_pap_password($password);
    
    $url = 'http://' . 
      hotspot_get_param('uamip') . ':' . 
      hotspot_get_param('uamport') . '/logon?username=' . 
      $username . '&password=' . $pappassword . '&userurl=' . 
	hotspot_get_param('userurl');
    
  }

  return $url;
}

function hotspot_wispr_xml($url) {
  return '<!--
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<WISPAccessGatewayParam 
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
  xsi:noNamespaceSchemaLocation=\"http://www.acmewisp.com/WISPAccessGatewayParam.xsd\">
<AuthenticationReply>
<MessageType>120</MessageType>
<ResponseCode>201</ResponseCode>
<LoginResultsURL>'.$url.'</LoginResultsURL>
</AuthenticationReply> 
</WISPAccessGatewayParam>
-->';
}

