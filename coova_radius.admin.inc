<?php


/**
 * Provides settings pages.
 */
function coova_radius_admin_settings() {

  $form['coova_radius'] = 
    array('#type' => 'fieldset',
	  '#title' => t('CoovaRADIUS'),
	  '#collapsible' => true,
	  '#collapsed' => false,
	  'coova_radius_service_name' =>
	  array('#type' => 'textfield',
		'#title' => t('Service name'),
		'#default_value' => variable_get('coova_radius_service_name', 'CoovaRADIUS'),
		'#size' => 30,
		'#maxlength' => 55,
		'#description' => t('The name of the RADIUS service.'),
		),
	  'coova_radius_provisioning' =>
	  array('#type' => 'radios',
		'#title' => t('CoovaRADIUS'),
		'#default_value' => variable_get('coova_radius_provisioning', 'none'),
		'#options' => array('none' => 'No automatic provisioning', 
				    'users' => 'Auto provision standard users',
				    'admins' => 'Auto provision administrative users',
				    'codes' => 'Auto issue access codes'),
		),
	  'coova_radius_cookie_key' =>
	  array('#type' => 'textfield',
		'#title' => t('Cookie Encryption Key'),
		'#default_value' => variable_get('coova_radius_cookie_key', ''),
		'#size' => 20,
		'#description' => t('Encryption key to use for cookie storage of voucher codes.'),
		),
	  'coova_radius_anon_provisioning' =>
	  array('#type' => 'radios',
		'#title' => t('Anonymous user access'),
		'#default_value' => variable_get('coova_radius_anon_provisioning', 'none'),
		'#options' => array('none' => 'No anonymous user access', 
				    'otp' => 'Anonymous user access using one-time password'),
		),
	  'coova_radius_anon_provisioning_attributes' =>
	  array('#type' => 'textarea',
		'#title' => t('Anonymous user access session attributes'),
		'#default_value' => variable_get('coova_radius_anon_provisioning_attributes', ''),
		),
	  array('#type' => 'fieldset',
		'#title' => t('CoovaRADIUS Advanced'),
		'#collapsible' => true,
		'#collapsed' => true,
		'coova_radius_provision_user_mac_auth' =>
		array('#type' => 'radios',
		      '#title' => t('Create users able to MAC Authenticate'),
		      '#default_value' => variable_get('coova_radius_provision_user_mac_auth', 'false'),
		      '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
		      ),
		'coova_radius_provision_user_realm' =>
		array('#type' => 'radios',
		      '#title' => t('Create a Realm for each user'),
		      '#default_value' => variable_get('coova_radius_provision_user_realm', 'false'),
		      '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
		      ),
		'coova_radius_provision_user_network' =>
		array('#type' => 'radios',
		      '#title' => t('Create a Network for each user'),
		      '#default_value' => variable_get('coova_radius_provision_user_network', 'false'),
		      '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
		      ),
		'coova_radius_paypal_enabled' =>
		array('#type' => 'radios',
		      '#title' => t('Enable Paypal'),
		      '#default_value' => variable_get('coova_radius_paypal_enabled', 'false'),
		      '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
		      ),
		'coova_radius_realm_id' =>
		array('#type' => 'textfield',
		      '#title' => t('Realm ID'),
		      '#default_value' => variable_get('coova_radius_realm_id', ''),
		      '#size' => 5,
		      '#maxlength' => 5,
		      '#description' => t('Numeric Realm ID in CoovaRADIUS Database.'),
		      ),
		'coova_radius_network_id' =>
		array('#type' => 'textfield',
		      '#title' => t('Network ID'),
		      '#default_value' => variable_get('coova_radius_network_id', ''),
		      '#size' => 5,
		      '#maxlength' => 5,
		      '#description' => t('Numeric Network ID in CoovaRADIUS Database.'),
		      ),
		'coova_radius_access_policy_id' =>
		array('#type' => 'textfield',
		      '#title' => t('Access Policy ID'),
		      '#default_value' => variable_get('coova_radius_access_policy_id', ''),
		      '#size' => 5,
		      '#maxlength' => 5,
		      '#description' => t('Numeric Access Policy ID in CoovaRADIUS Database.'),
		      ),
		'coova_radius_attribute_set_id' =>
		array('#type' => 'textfield',
		      '#title' => t('Attribute Set ID'),
		      '#default_value' => variable_get('coova_radius_attribute_set_id', ''),
		      '#size' => 5,
		      '#maxlength' => 5,
		      '#description' => t('Numeric Attribute Set ID in CoovaRADIUS Database.'),
		      ),
		),
	  array('#type' => 'fieldset',
		'#title' => t('CoovaRADIUS Menus'),
		'#collapsible' => true,
		'#collapsed' => true,
		'coova_radius_user_usage' =>
		array('#type' => 'radios',
		      '#title' => t('Have a Usage tab for when not using EWT GUI'),
		      '#default_value' => variable_get('coova_radius_user_usage', 'true'),
		      '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
		      ),
		),
	  );

  return system_settings_form($form);
}

