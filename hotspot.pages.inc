<?php

function hotspot_render($tid = 0) {
  $tid = (int) $tid;

  require_once drupal_get_path('module', 'hotspot').'/hotspot.inc';

  if (isset($_REQUEST['uamip'])) {

    module_invoke_all('hotspot_loginpage');

    if (variable_get('hotspot_check_url_md', 'true') == 'true') {
      $md = $_REQUEST['md'];
      $uamsecret = hotspot_get_uamsecret();
      $check = 'http://'.$_SERVER['SERVER_NAME'].preg_replace('/&md=.*$/', '', $_SERVER['REQUEST_URI']);
      $match = strtoupper(md5($check.$uamsecret));
      if ($md != $match) {
	drupal_goto('badquery');
	return;
      }
    }

    $_SESSION['hotspot'] = 'true';
    $_SESSION['uamqs'] = $_SERVER['QUERY_STRING'];
    $_SESSION['logouturl'] = "http://".$_REQUEST['uamip'].":".$_REQUEST['uamport']."/logout";
    hotspot_set_session('uamip');
    hotspot_set_session('uamport');
    hotspot_set_session('challenge');
    hotspot_set_session('userurl');
    hotspot_set_session('called');
    hotspot_set_session('mac');
    hotspot_set_session('res');
  }

  /**
   *  If not using the JavaScript controller, and with auto-provisioning on,
   *  then kick-off authentication. 
   */

  if ($_REQUEST['res'] == 'notyet') {
    if (variable_get('hotspot_method', 'js') != 'js') {
      if (variable_get('hotspot_provisioning', 'none') == 'anonymoususer') {
	$username = variable_get('hotspot_auto_username', '');
	$password = variable_get('hotspot_auto_password', '');
      }
    } 
  }
  
  if ($_REQUEST['res'] == 'splash') {
  }
  
  if ($item = module_invoke_all('hotspot_dologin')) {
    if (is_array($item)) {
      $username = $item['username'];
      $password = $item['password'];
    }
  }
  
  /**
   *  If this is a login attempt, we'll process that here so that
   *  we can issue a redirect back to CoovaChilli. (Not use by
   *  JavaScript controller)
   */

  if (!$username)
    $username = hotspot_get_username($_REQUEST);

  if ($username) {

    if (!$password) 
      $password = hotspot_get_password($_REQUEST);

    $url = hotspot_login_url($username, $password);

    header('Location: '. $url, TRUE, 302);

    print hotspot_wispr_xml($url);

    exit();
  }

  if ($_REQUEST['res'] == 'notyet') {
    if (variable_get('hotspot_initial_redirect', '') != '') {
      drupal_goto(variable_get('hotspot_initial_redirect', ''));
    }
  }
  
  if ($_REQUEST['res'] == 'success') {
    if (variable_get('hotspot_success_redirect', '') != '') {
      drupal_goto(variable_get('hotspot_success_redirect', ''));
    }
  }

  if ($_SESSION['renderNode']) {
    $node = node_load(array('nid' => $_SESSION['renderNode']));
    print theme('hotspot_page', $node);
    return;
  }

  print theme('page', hotspot_render_loginform());
}

function hotspot_render_loginform($tid = 0) {

  if (hotspot_get_param('uamip') == '') {
    $content = hotspot_content($tid, 'nochilli');
  } else if (variable_get('hotspot_enabled', 'true') != 'true') {
    $content = hotspot_content($tid, 'disabled');
  } else if (variable_get('hotspot_method', 'js') == 'js') {
    $content = hotspot_content($tid, 'jshtml');
  } else {
    $content = hotspot_content($tid, hotspot_get_param('res'));
  }

  return $content;
}

function theme_hotspot_page(&$node) 
{
  return node_view($node, false, true);
}

function theme_hotspot_disabled(&$node) 
{
  return '<p>HotSpot services are currently not available.</p>';
}

function theme_hotspot_notyet(&$node) 
{
  $userAgent = $_SERVER['HTTP_USER_AGENT'];
  if (preg_match("/profile\/midp/i", $userAgent) || preg_match("/configuration\/cldc/i", $userAgent)) {
    return theme_hotspot_loginform($node);
  } else {
    return theme('hotspot_loginform', $node);
  }
}

function theme_hotspot_failed(&$node) 
{
  $reply = $_GET['reply'];
  if ($reply == '') $reply = 'Authentication failed';
  return '<p>'.$reply.'</p>'.theme('hotspot_loginform', $node);
}

function theme_hotspot_already(&$node) 
{
  return theme('hotspot_success', $node);
}

function theme_hotspot_success(&$node) 
{
  return '<p>You are now on-line.</p>
<p><a href="'.$_SESSION['userurl'].'">'.$_SESSION['userurl'].'</a></p>';
}

function theme_hotspot_loginform(&$node) 
{
  $html = '<div id="hotLoginForm">';

  if (variable_get('hotspot_loginform', 'true') == 'true') {
    $html .= '<form id="hotForm" name="form1" method="get">
<input type="hidden" name="q" value="hotspot">
<input type="hidden" name="res" value="notyet">
<input type="hidden" name="challenge" value="' . hotspot_get_param('challenge') . '">
<input type="hidden" name="userurl" value="' .hotspot_get_param('userurl') . '">
<table id="hotLoginFormTable" border="0" cellpadding="5" cellspacing="0">
<tbody><tr>
<td class="hotLoginFormLabel hotUsernameLabel">Username:</td>
<td class="hotLoginFormInput hotUsernameInput"><input id="hotUsername" type="text" name="UserName" size="20" maxlength="255"></td>
</tr><tr>
<td class="hotLoginFormLabel hotPasswordLabel">Password:</td>
<td class="hotLoginFormInput hotPasswordInput"><input id="hotPassword" type="password" name="Password" size="20" maxlength="255"></td>
</tr><tr>
<td class="hotLoginFormSubmit" colspan="2"><input id="hotSubmit" type="submit" name="login" value="login"></td>
</tr></tbody>
</table>
</form>';
  }

  if (variable_get('hotspot_code_enabled', 'false') == 'true')
    $html .= '<form name="form2" method="get">
<input type="hidden" name="q" value="hotspot">
<input type="hidden" name="res" value="notyet">
<input type="hidden" name="challenge" value="' . hotspot_get_param('challenge') . '">
<input type="hidden" name="userurl" value="' .hotspot_get_param('userurl') . '">
<input type="hidden" name="realm" value="' .variable_get('hotspot_code_realm', '') . '">
<table border="0" cellpadding="5" cellspacing="0" style="width: 217px;">
<tbody><tr>
<td nowrap="nowrap" align="right">Access code:</td>
<td><input type="text" name="UserName" size="16" maxlength="16"></td>
<td><input type="password" name="Password" size="16" maxlength="16"></td>
</tr><tr>
<td align="center" colspan="2" height="23"><input type="submit" name="login" value="login"></td>
</tr></tbody>
</table>
</form>';

  if ($a = module_invoke_all('hotspot_links'))
    foreach ($a as $b) $html .= $b;

  return $html;
}

function theme_hotspot_nochilli(&$node) 
{
  return 'You are not at a hotspot location.';
}

function theme_hotspot_jshtml(&$node) 
{
  $loginTab = '';
  $codeTab = '';
  $selected = '-selected';
  $u = '';
  $p = '';

  if (variable_get('hotspot_loginform', 'true') == 'true') {
    $loginTab='<td class="tableTabItem'.$selected.'" id="tabAccount" onClick="javascript:return tab(\'Account\');">User Login</td>';
    $u='Username';
    $p='Password';
    $selected = '';
  }

  if (variable_get('hotspot_code_enabled', 'false') == 'true') {
    $codeTab='<td class="tableTabItem'.$selected.'" id="tabCode" onClick="javascript:return tab(\'Code\');">Access Code</td>';
    if ($u == '') {
      $u='Code';
    }
  }

  return '<div id="chilliPage">

<div id="logonPage" style="display:none;">
<form onSubmit="return false;" name="logon">

<table id="tableTab" cellpadding="0" cellspacing="0">
<tr><td nowrap="nowrap" class="navRow">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="tableTabFirst">&nbsp;</td>
'.$loginTab.$codeTab.'
<td class="tableTabLast">&nbsp;</td>
</tr>
</table>

</td>
</tr>
<tr>
<td class="tableTabBottom">

<div class="usernameRow">
<span id="usernameLabel">'.$u.'</span>
<input name="username" type="text" id="username" size="10"/>
<span id="passwordField"><span id="passwordLabel">'.$p.'</span>
<input name="password" type="password" id="password" size="10"/></span>
</div>

<div class="connectRow">
<input id="connectButton" type="submit" value="Connect" onClick="connect();"/><span id="logonMessage">Click \'Connect\' to login</span>
</div>

</td>
</tr>
<tr>
<td align="right" style="font-size: 8px;padding: 0px 2px 1px 0px;">Powered by <a href="http://coova.org/" target="_blank">coova.org</a>&nbsp;<a href="http://coova.org/" target="_blank"><img src="https://coova.org/images/icon.gif" border="0"/></a>&nbsp;</td>
</tr>
</table>
</form>
</div>

<div id="statusPage" style="display:none;">

<table id="tableTab" cellpadding="0" cellspacing="0">
<tr>
<td class="navRow">
<table id="navTable" cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="tableTabFirst">&nbsp;</td>
<td class="tableTabItem-selected" id="tabStatus" nowrap="nowrap" onClick="javascript:return statusTab(\'Status\');">Status</td>
<td class="tableTabLast">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="tableTabBottom">

<table border="0" id="statusTable" style="padding-top:4px;">
<tr id="connectRow">
<td><span id="statusMessage">Connected</span></td>
<td><a href="#" onClick="return disconnect();">logout</a></td>
</tr>
<tr id="sessionIdRow">
<td id="sessionIdLabel" class="chilliLabel">Session ID</td>
<td id="sessionId" class="chilliValue">Not available</td>
</tr>
<tr id="startTimeRow">
<td id="startTimeLabel" class="chilliLabel">Start Time</td>
<td id="startTime" class="chilliValue">Not available</td>
</tr>
<tr id="sessionTimeRow">
<td id="sessionTimeLabel" class="chilliLabel">Session Time</td>
<td id="sessionTime" class="chilliValue">
<span id="time">Not available</span>
<span id="timelimit"><!-- --></span>
<span id="timeleft"><!-- --></span>
</td>
</tr>
<tr id="idleTimeRow">
<td id="idleTimeLabel" class="chilliLabel">Idle Time</td>
<td id="idleTime" class="chilliValue">
<span id="idle">Not available</span>
<span id="idlelimit"><!-- --></span>
<span id="idleleft"><!-- --></span>
</td>
</tr>
<tr id="downOctetsRow">
<td id="downOctetsLabel" class="chilliLabel">Downloaded</td>
<td id="downOctets" class="chilliValue">
<span id="down">Not available</span>
<span id="downlimit"><!-- --></span>
<span id="downleft"><!-- --></span>
</td>
</tr>
<tr id="upOctetsRow">
<td id="upOctetsLabel" class="chilliLabel">Uploaded</td>
<td id="upOctets" class="chilliValue">
<span id="up">Not available</span>
<span id="uplimit"><!-- --></span>
<span id="upleft"><!-- --></span>
</td>
</tr>
<tr id="originalURLRow">
<td id="originalURLLabel" class="chilliLabel">Original URL</td>
<td id="originalURL" class="chilliValue">N/A</td>
</tr>
</table>
</td></tr>
<tr>
<td colspan="3" align="right" style="font-size: 8px;padding: 0px 2px 1px 0px;">Authentication services by <a href="http://coova.org/" target="_blank">coova.org</a>&nbsp;<a href="http://coova.org/" target="_blank"><img src="https://coova.org/images/icon.gif" border="0"/></a>&nbsp;</td>
</tr>
</table>
</div>

<div id="waitPage" style="display:none;">
Please wait...
</div>

<div id="errorPage" style="display:none;">
<span id="errorMessage">Error</span>
</div>
</div>';
}

function theme_hotspot_block(&$node) 
{
  return '<div id="chilliPage">
<div id="logonPage" style="display:none;">
You are not logged in.
<a href="?q=hotspot">Login now</a>
</form>
</div>

<div id="statusPage" style="display:none;">

<div>
<span id="statusMessage">Connected</span> -
<a href="#" onClick="return disconnect();">logout</a>
</div>

<div>
<span id="sessionTimeLabel" class="chilliLabel">Session Time</span>
<span id="sessionTime" class="chilliValue">
<span id="time">Not available</span>
<span id="timelimit"><!-- --></span>
<span id="timeleft"><!-- --></span>
</span>
</div>

</div>

<div id="waitPage" style="display:none;">
Please wait...
</div>

<div id="errorPage" style="display:none;">
<span id="errorMessage">Error</span>
</div>
</div>';

}

