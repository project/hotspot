<?php


/**
 * Provides settings pages.
 */
function hotspot_admin_settings() {

  $form['server'] = 
    array('#type' => 'fieldset',
	  '#title' => t('Hotspot settings'),
	  '#collapsible' => true,
	  '#collapsed' => false,
	  );
  
  $form['server']['hotspot_enabled'] = 
    array('#type' => 'radios',
	  '#title' => t('Hotspot module'),
	  '#default_value' => variable_get('hotspot_enabled', 'true'),
	  '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
	  );

  $form['server']['hotspot_check_url_md'] = 
    array('#type' => 'radios',
	  '#title' => t('Check URL for tampering'),
	  '#default_value' => variable_get('hotspot_check_url_md', 'true'),
	  '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
	  );

  $form['server']['hotspot_method'] = 
    array('#type' => 'radios',
	  '#title' => t('Method of login'),
	  '#default_value' => variable_get('hotspot_method', 'redir'),
	  '#options' => array('redir' => 'Browser Redirects', 'js' => 'JavaScript Controller'),
	  );

  $form['server']['hotspot_initial_redirect'] = 
    array('#type' => 'textfield',
	  '#title' => t('Redirect initial request to page'),
	  '#default_value' => variable_get('hotspot_initial_redirect', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Drupal page to redirect to (e.g. <i>node/10</i>), blank for none.'),
	  );

  $form['server']['hotspot_success_redirect'] = 
    array('#type' => 'textfield',
	  '#title' => t('Redirect to page on login success'),
	  '#default_value' => variable_get('hotspot_success_redirect', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Drupal page to redirect to (e.g. <i>node/10</i>), blank for none.'),
	  );

  $form['server']['hotspot_uamsecret'] = 
    array('#type' => 'textfield',
	  '#title' => t('UAM Secret'),
	  '#default_value' => variable_get('hotspot_uamsecret', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('UAM Secret used in chilli - <b>assumed blank for JavaScript Controller</b>.'),
	  );
  
  $form['server']['hotspot_authproto'] = 
    array('#type' => 'radios',
	  '#title' => t('RADIUS Authentication Protocol'),
	  '#default_value' => variable_get('hotspot_authproto', 'chap'),
	  '#options' => array( 'chap' => 'CHAP', 'pap' => 'PAP'),
	  '#description' => t('JavaScript Controller will always use CHAP.'),
	  );
  
  $form['provisioning'] = 
    array('#type' => 'fieldset',
	  '#title' => t('Access provisioning'),
	  '#collapsible' => true,
	  '#collapsed' => true,
	  );
  
  $form['provisioning']['hotspot_loginform'] = 
    array('#type' => 'radios',
	  '#title' => t('User login form'),
	  '#default_value' => variable_get('hotspot_loginform', 'true'),
	  '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
	  );
  
  $form['provisioning']['hotspot_provisioning'] = 
    array('#type' => 'radios',
	  '#title' => t('User auto provisioning'),
	  '#default_value' => variable_get('hotspot_provisioning', 'none'),
	  '#options' => array('none' => 'No automatic provisioning', 
			      'anonymoususer' => 'Auto Login as the user defined below'),
	  );

  $form['provisioning']['hotspot_realm'] = 
    array('#type' => 'textfield',
	  '#title' => t('User realm'),
	  '#default_value' => variable_get('hotspot_realm', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Realm used when authenticating users, blank for none.'),
	  );
  
  $form['provisioning']['hotspot_auto_username'] = 
    array('#type' => 'textfield',
	  '#title' => t('HotSpot anonymous user username'),
	  '#default_value' => variable_get('hotspot_auto_username', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Username to login with during "anonymoususer" provisioning.'),
	  );
  
  $form['provisioning']['hotspot_auto_password'] = 
    array('#type' => 'textfield',
	  '#title' => t('HotSpot anonymous user password'),
	  '#default_value' => variable_get('hotspot_auto_password', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Password to login with during "anonymoususer" provisioning.'),
	  );
  
  $form['codes'] = 
    array('#type' => 'fieldset',
	  '#title' => t('Access codes'),
	  '#collapsible' => true,
	  '#collapsed' => true,
	  );
  
  $form['codes']['hotspot_code_enabled'] = 
    array('#type' => 'radios',
	  '#title' => t('Access codes'),
	  '#default_value' => variable_get('hotspot_code_enabled', 'false'),
	  '#options' => array('false' => 'Disabled', 'true' => 'Enabled'),
	  );
  
  $form['codes']['hotspot_code_realm'] = 
    array('#type' => 'textfield',
	  '#title' => t('HotSpot access code realm'),
	  '#default_value' => variable_get('hotspot_code_realm', ''),
	  '#size' => 30,
	  '#maxlength' => 55,
	  '#description' => t('Realm to use with access code.'),
	  );
  
  return system_settings_form($form);
}

