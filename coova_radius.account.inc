<?php

function coova_radius_settings($user) {
  if (ewt_proxy_enabled(true)) {
    return ewt_wait_div().ewt_menu_div();
  }
  
  return drupal_get_form('ewt_form', 'drupal-settings', 
			 variable_get('coova_radius_service_name', 'CoovaRADIUS').' Settings');
}


function coova_radius_ewt_usage($user) {
  if (ewt_proxy_enabled(true)) {
    return ewt_wait_div().ewt_div('drupal-usage');
  }
  
  return drupal_get_form('ewt_form', 'drupal-sessions', 
			 variable_get('coova_radius_service_name', 'CoovaRADIUS').' Usages');
}

